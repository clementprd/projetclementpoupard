# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'projetclementpoupard',
    'version' : '0.1',
    'summary': 'projetclementpoupard',
    'sequence': 10,
    'license' : 'LGPL-3',
    'depends' : ['base'
    ],
    'data': [
        'views/iut_class_view.xml',
        'views/iut_course_view.xml',
        'views/iut_schedule_view.xml',
        'views/iut_student_view.xml',
        'views/res_partner_view.xml',
        'menu_view.xml',
    ],
    'demo': [
        #'demo/.xml',
    ],
    'qweb': [
        #"static/src/xml/.xml",
        
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'post_load': None,
    'pre_init_hook': None,
    'post_int_hook': None,
}
