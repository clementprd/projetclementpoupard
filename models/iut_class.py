from odoo import api, fields, models

class IutClass(models.Model):
    _name = "iut.class"
    _description = "Classes"
    
    name = fields.Char('Nom',required=True)
    level = fields.Selection([('second','Seconde'),('first','Première'),('final','Terminale')],string='Niveau scolaire',required=True)
    teacher_ids = fields.Many2many('res.partner',string='Professeurs')
    student_ids = fields.One2many('iut.student','class_id',string='Élèves')
    student_nb = fields.Integer("Nombre d'élèves",compute='countstudent')
    courses_ids = fields.One2many('iut.schedule','class_id',string='Cours')
    
    @api.depends()
    def countstudent(self):
        for record in self:
            if record.student_ids != False:
                compte_student = 0
                for rec in record.student_ids:
                    compte_student = compte_student + 1
                record.student_nb = compte_student
            else:
                record.student_nb = 0
        
        