from odoo import api, fields, models

class IutCourse(models.Model):
    _name = "iut.course"
    _description = "Cours"
    
    name = fields.Char('Nom cours',required=True)