from odoo import api, fields, models

class IutSchedule(models.Model):
    _name = "iut.schedule"
    _description = "Agenda"
    
    date_start = fields.Datetime('Horaire début',required=True)
    date_stop = fields.Datetime('Horaire fin',required=True)
    room = fields.Char('Salle de classe')
    class_id = fields.Many2one('iut.class',string='classe')
    course_id = fields.Many2one('iut.course',string='Cours')
    name = fields.Char('Nom du cours',compute='gen_name')
    
    @api.depends('class_id','course_id','room')
    def gen_name(self):
        for rec in self: 
            if rec.class_id.id != False and rec.course_id.id != False and rec.room != False:
                rec.name = rec.course_id.name + ',' + rec.class_id.name + ',' + rec.room
            else:
                rec.name = "Sans nom"