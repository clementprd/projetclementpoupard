from odoo import api, fields, models
from datetime import date
from dateutil.relativedelta import relativedelta

class IutStudent(models.Model):
    _name = "iut.student"
    _description = "Élèves"
    
    firstname = fields.Char('Nom',required=True)
    lastname = fields.Char('Prenom',required=True)
    birthdate = fields.Date('Date de naissance',required=True)
    age = fields.Integer('Age',compute="calculate_age")
    class_id = fields.Many2one('iut.class',string='Classe') 
    
    @api.depends('birthdate')
    def calculate_age(self):
        for rec in self: 
            if rec.birthdate != False:
                d1 = rec.birthdate
                d2 = date.today()
                d3 = d2-d1
                rec.age =  d3.days / 365.25
            else:
                rec.age = 0
    
    


